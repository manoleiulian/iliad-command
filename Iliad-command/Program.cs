﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
namespace Iliad_command
{
    class Program {
        
        static async System.Threading.Tasks.Task Main(string[] args)
        {  /*Questo codice funziona sotto rete Iliad e anche da qualsiasi rete Normale*/
            Console.OutputEncoding = Encoding.UTF8;
            String URI = "https://www.iliad.it/account/";
            HttpClient client = new HttpClient();
            string DIRECT_POST_CONTENT_TYPE = "application/x-www-form-urlencoded";
            string postData = "login-ident=YOUR-ACCOUNTID&login-pwd=YOUR-PASSWORD";
            client.DefaultRequestHeaders.Add("Cookie", "auth_mobile=1;");
            StringContent content = new StringContent(postData, Encoding.UTF8, DIRECT_POST_CONTENT_TYPE);
            HttpResponseMessage response = await client.PostAsync(URI,content);
            string result = await response.Content.ReadAsStringAsync();

            /* Scrapping dei dati dal Html ricevuto come risposta 
             -Nome_Cognome = I dati anagrafici del cleinte
             -Numero = Numero di telefono del cliente
             -Sms = Numero dei sms inviati
             -Mms = Numero dei mms inviati
             -Chiamte = Il tempo trascorso in chiamate
             -Credito = Il Credito del numero
             -Dati Utilizzati = Il numero di GB utilizzati
             -Dati Totali = Il numero di GB disponibili dell'offerta
             -Rinnovo = Data di rinnova dell'offerta
             EXTRA:
             -Dati-Rim = Il numero di GB ancora rimanenti (GB,MB)
             */
            string Nome_Cognome = Regex.Match(result, @"<div class=""bold"">(.*)</div>").Groups[1].Value;
            string Numero = Regex.Match(result, @"<div class=""smaller"">Numero:(.*)</div>").Groups[1].Value;
            string Sms = Regex.Match(result, @"<div class=""conso__text""><span class=""red"">(\d+) SMS</span>").Groups[1].Value;
            string Mms= Regex.Match(result, @"<span class=""red"">(\d+) MMS<br></span>").Groups[1].Value;
            string ID_Utente = Regex.Match(result, @"<div class=""smaller"">ID utente:(.*)</div>").Groups[1].Value;
            string Chimate = Regex.Match(result, @"Chiamate: <span class=""red"">(.*)</span><br>").Groups[1].Value;
            string Dati_Utilizzati = Regex.Match(result, @"<span class=""red"">(.*)</span> / (.*)<br>").Groups[1].Value;
            string Credito = Regex.Match(result, @"<b class=""red"">(\d+.+€+)\</b>").Groups[1].Value;
            string Rinnovo = Regex.Match(result, @"La tua offerta iliad si rinnoverà alle (.*)").Groups[1].Value;
            string Dati_Totali = Regex.Match(result, @"<span class=""red"">(.*)</span> / (.*)<br>").Groups[2].Value;
            string tmp_Tot = Regex.Replace(Dati_Totali, "GB", "");
            string tmp_Util= Regex.Replace(Dati_Utilizzati, "GB", "");
            /* Calcolo dei GB Rimanenti*/
            float Dati_Rim = 0;
            /* Try & Catch in caso qualcosa va storto durante login ecc*/
            try
            {
                Dati_Rim = float.Parse(tmp_Tot) - float.Parse(tmp_Util);
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Errore Stringa o nessun valore\n");
            }

            Console.WriteLine("ID UTENTE: "+ID_Utente);
            Console.WriteLine("Numero: " + Numero);
            Console.WriteLine("Credito: " + Credito);
            Console.WriteLine("SMS NAZIONALI: "+Sms);
            Console.WriteLine("MMS NAZIONALI: " + Mms);
            Console.WriteLine("Nome Cognome : "+Nome_Cognome);
            Console.WriteLine("Chiamate : " + Chimate);
            Console.WriteLine("Dati Utilizzati: " + Dati_Utilizzati);
            Console.WriteLine("DATI TOTALI: " + Dati_Totali);
            Console.WriteLine("Dati Rimanenti: " +Dati_Rim+"GB");
            Console.WriteLine("Rinnovo Offerta alle: " + Rinnovo);

            
        }
    }
}

